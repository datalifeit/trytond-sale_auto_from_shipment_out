# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    sale_from_shipment = fields.Boolean('Sale from shipment',
        states={'readonly': ~Eval('active')},
        depends=['active'],
        help='If checked Sale will be created automatically from Shipment out')
    sale_from_shipment_return = fields.Boolean('Sale from shipment return',
        states={'readonly': ~Eval('active')},
        depends=['active'],
        help='If checked Sale will be created automatically from Shipment '
        'out return')

    @staticmethod
    def default_sale_from_shipment():
        return True

    @staticmethod
    def default_sale_from_shipment_return():
        return True
