# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from .test_sale_auto_from_shipment_out import suite

__all__ = ['suite']
